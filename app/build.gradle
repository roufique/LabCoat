buildscript {
    repositories {
        maven { url 'https://maven.fabric.io/public' }
    }

    dependencies {
        classpath 'io.fabric.tools:gradle:1.25.4'
    }
}

repositories {
    maven { url 'https://maven.fabric.io/public' }
}

apply plugin: 'com.android.application'

apply plugin: 'kotlin-android'

apply plugin: 'kotlin-kapt'

apply plugin: 'kotlin-android-extensions'

apply plugin: 'io.fabric'

android {

    project.ext {
        LABCOAT_FABRIC_KEY = project.hasProperty("LABCOAT_FABRIC_KEY") ? project.LABCOAT_FABRIC_KEY : ""
        KEYSTORE_PATH = project.hasProperty("KEYSTORE_PATH") ? project.KEYSTORE_PATH : " "
        KEYSTORE_PASSWORD = project.hasProperty("KEYSTORE_PASSWORD") ? project.KEYSTORE_PASSWORD : " "
        KEY_PASSWORD = project.hasProperty("KEY_PASSWORD") ? project.KEY_PASSWORD : " "
    }

    compileSdkVersion 28

    defaultConfig {
        applicationId "com.commit451.gitlab"
        minSdkVersion 16
        targetSdkVersion 28
        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
        versionCode 2060400
        versionName "2.6.4"
        manifestPlaceholders = [fabric_key: project.ext.LABCOAT_FABRIC_KEY]
        vectorDrawables.useSupportLibrary = true

        multiDexEnabled true
    }

    flavorDimensions "type"

    productFlavors {

        normal {
            dimension "type"
        }

        fdroid {
            dimension "type"
        }
    }

    signingConfigs {
        release {
            storeFile file(project.ext.KEYSTORE_PATH)
            storePassword project.ext.KEYSTORE_PASSWORD
            keyAlias "commit451"
            keyPassword project.ext.KEY_PASSWORD
        }
    }

    buildTypes {
        release {
            minifyEnabled true
            shrinkResources true
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
            ext.enableCrashlytics = true
            signingConfig signingConfigs.release
        }
        debug {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
            ext.enableCrashlytics = false
            applicationIdSuffix ".debug"
            versionNameSuffix "-debug"
        }
    }

    packagingOptions {
        exclude 'META-INF/MANIFEST.MF'
        exclude 'META-INF/app_fdroidDebug.kotlin_module'
    }
}

ext {
    retrofitVersion = '2.4.0'
    okHttpVersion = '3.11.0'
    butterknifeVersion = '9.0.0-SNAPSHOT'
    parcelerVersion = '1.1.11'
    reptarVersion = '2.5.1'
    adapterLayout = '1.1.2'
    materialDialogsVersion = '0.9.6.0'
    leakCanaryVersion = '1.6.1'
    addendumVersion = '2.1.0'
    moshiVersion = '1.6.0'
    autodisposeVersion = '0.8.0'
}

dependencies {
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion"

    implementation 'androidx.core:core-ktx:1.0.0'

    implementation 'androidx.appcompat:appcompat:1.0.0'
    implementation 'androidx.recyclerview:recyclerview:1.0.0'
    implementation 'androidx.cardview:cardview:1.0.0'
    implementation 'androidx.palette:palette:1.0.0'
    implementation 'androidx.browser:browser:1.0.0'
    implementation 'androidx.multidex:multidex:2.0.0'

    implementation 'com.google.android.material:material:1.0.0'

    implementation "com.squareup.retrofit2:retrofit:$retrofitVersion"
    implementation("com.squareup.retrofit2:converter-simplexml:$retrofitVersion") {
        exclude group: 'xpp3', module: 'xpp3'
        exclude group: 'stax', module: 'stax-api'
        exclude group: 'stax', module: 'stax'
    }
    implementation "com.squareup.retrofit2:converter-scalars:$retrofitVersion"
    implementation "com.squareup.retrofit2:adapter-rxjava2:$retrofitVersion"
    implementation "com.squareup.retrofit2:converter-moshi:$retrofitVersion"
    implementation "com.squareup.okhttp3:okhttp:$okHttpVersion"
    implementation "com.squareup.okhttp3:logging-interceptor:$okHttpVersion"
    implementation 'com.squareup.picasso:picasso:2.5.2'
    implementation "com.squareup.moshi:moshi:$moshiVersion"
    implementation "com.squareup.moshi:moshi-adapters:$moshiVersion"

    implementation 'com.jakewharton.picasso:picasso2-okhttp3-downloader:1.1.0'
    implementation "com.jakewharton:butterknife:$butterknifeVersion"
    kapt "com.jakewharton:butterknife-compiler:$butterknifeVersion"
    implementation 'com.jakewharton.timber:timber:4.7.1'
    implementation 'com.jakewharton.threetenabp:threetenabp:1.1.0'

    implementation 'org.greenrobot:eventbus:3.1.1'

    implementation 'io.reactivex.rxjava2:rxjava:2.2.2'
    implementation 'io.reactivex.rxjava2:rxandroid:2.1.0'

    implementation "com.uber.autodispose:autodispose-kotlin:$autodisposeVersion"
    implementation "com.uber.autodispose:autodispose-android-kotlin:$autodisposeVersion"
    implementation "com.uber.autodispose:autodispose-android-archcomponents-kotlin:$autodisposeVersion"

    implementation "org.parceler:parceler-api:$parcelerVersion"
    kapt "org.parceler:parceler:$parcelerVersion"

    implementation "com.github.Commit451.Reptar:reptar:$reptarVersion"
    implementation "com.github.Commit451.Reptar:reptar-retrofit:$reptarVersion"
    implementation "com.github.Commit451.Reptar:reptar-kotlin:$reptarVersion"
    implementation 'com.github.Commit451:ElasticDragDismissLayout:1.0.4'
    implementation "com.github.Commit451.AdapterLayout:adapterlayout:$adapterLayout"
    implementation("com.github.Commit451.AdapterLayout:adapterflowlayout:$adapterLayout") {
        exclude group: 'com.wefika', module: 'flowlayout'
    }
    //https://github.com/blazsolar/FlowLayout/issues/31
    implementation("com.wefika:flowlayout:0.4.1") {
        exclude group: 'com.intellij', module: 'annotations'
    }
    implementation 'com.github.Commit451:Easel:3.0.0'
    implementation 'com.github.Commit451:Gimbal:2.0.2'
    implementation 'com.github.Commit451:Teleprinter:2.2.0'
    implementation 'com.github.Commit451:Jounce:1.0.2'
    implementation 'com.github.Commit451:ForegroundViews:2.4.4'
    implementation 'com.github.Commit451:MorphTransitions:2.0.0'
    implementation "com.github.Commit451:Alakazam:2.0.0"
    implementation 'com.github.Commit451:Lift:2.0.1'
    implementation 'com.github.Commit451:okyo:3.0.2'
    implementation 'com.github.Commit451:Aloy:1.2.0'
    implementation "com.github.Commit451.Addendum:addendum:$addendumVersion"
    implementation "com.github.Commit451.Addendum:addendum-recyclerview:$addendumVersion"
    implementation "com.github.Commit451.Addendum:addendum-design:$addendumVersion"
    implementation "com.github.Commit451.Addendum:addendum-parceler:$addendumVersion"

    implementation 'com.github.chrisbanes:PhotoView:2.2.0'

    implementation 'me.zhanghai.android.materialprogressbar:library:1.4.2'

    implementation 'com.github.Jawnnypoo:PhysicsLayout:2.1.0'

    implementation 'com.github.ivbaranov:materiallettericon:0.2.3'

    implementation 'com.github.alorma:diff-textview:1.3.0'

    implementation 'com.wdullaer:materialdatetimepicker:3.6.4'

    implementation 'com.github.novoda:simple-chrome-custom-tabs:0.1.6'

    implementation "com.afollestad.material-dialogs:core:$materialDialogsVersion"
    implementation "com.afollestad.material-dialogs:commons:$materialDialogsVersion"

    implementation 'de.hdodenhof:circleimageview:2.2.0'

    implementation('com.vdurmont:emoji-java:4.0.0') {
        exclude group: 'org.json', module: 'json'
    }

    implementation 'com.github.jkwiecien:EasyImage:2.1.0'

    implementation 'com.atlassian.commonmark:commonmark:0.11.0'

    normalImplementation 'com.crashlytics.sdk.android:crashlytics:2.9.5'

    debugImplementation "com.squareup.leakcanary:leakcanary-android:$leakCanaryVersion"
    releaseImplementation "com.squareup.leakcanary:leakcanary-android-no-op:$leakCanaryVersion"
    testImplementation "com.squareup.leakcanary:leakcanary-android-no-op:$leakCanaryVersion"

    testImplementation 'junit:junit:4.12'
    testImplementation('org.threeten:threetenbp:1.3.6') {
        exclude group: 'com.jakewharton.threetenabp', module: 'threetenabp'
    }
}
